<?php
$x = 0;

while ($x <= 50) {
    echo "$x - Repetido <br>";
    $x = $x + 5;
    $x += 5;
}

echo '<hr>';

$y = 0;

do {
    echo "$y - Repetido com do While<br>";
    $y ++;
} while ($y <= 50);