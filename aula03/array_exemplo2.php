<?php

$aluno1 = [
    'nome' => 'Cristina Souza Terra',
    'turma' => '2B',
    'materia' => [
        'nome' => 'Matemática',
        'notas' => [
            'bimestre1' => 8,
            'bimestre2' => 6,
            'bimestre3' => 9,
            'bimestre4' => 7
        ]
    ]
];

$aluno2 = [
    'nome' => 'Cintia Souza Costa',
    'turma' => '2B',
    'materia' => [
        'nome' => 'Matemática',
        'notas' => [
            'bimestre1' => 5,
            'bimestre2' => 7,
            'bimestre3' => 8,
            'bimestre4' => 8
        ]
    ]
];

$alunos = [$aluno1, $aluno2];

// echo '<pre>';
// print_r($alunos);

// foreach ($alunos as $chave => $aluno) {
//     echo 'Nome: ' . $aluno['nome'] . '<br>';
//     echo 'Turma: ' . $aluno['turma'] . '<br>';
//     echo 'Materia: ' . $aluno['materia']['nome'] . '<br>';
//     echo 'Notas: ' . $aluno['materia']['notas']['bimestre1']
//                    . ' | ' . $aluno['materia']['notas']['bimestre2']
//                    . ' | ' . $aluno['materia']['notas']['bimestre3']
//                    . ' | ' . $aluno['materia']['notas']['bimestre4'];
    
//     $media = (  $aluno['materia']['notas']['bimestre1'] + 
//                 $aluno['materia']['notas']['bimestre2'] +
//                 $aluno['materia']['notas']['bimestre3'] +
//                  $aluno['materia']['notas']['bimestre4']
//               ) / 4;
    
//     echo "<br>Média: $media<br>";
    
//     echo '<hr>';
// }


foreach ($alunos as $aluno){
    echo 'Nome: ' . $aluno['nome'] . '<br>';
    echo 'Turma: ' . $aluno['turma'] . '<br>';
    echo 'Materia: ' . $aluno['materia']['nome'] . '<br>';
    $total = 0;
    $notas = '';
    
    foreach ($aluno['materia']['notas'] as $nota){
       $total += $nota;
       $notas .= "$nota | ";
    }
    
    $media = $total / 4;
    
    echo "Notas: $notas <br>";
    echo "Media: $media";
    
    echo '<hr>';
}

/**
 * Escrever um algoritmo que imprime:
 * Nome / Turma / Materia / Notas dos Bimestres / Média Final
*/