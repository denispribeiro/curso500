<?php

$aluno = '';
$bimestre1 = 10;
$bimestre2 = 10;
$bimestre3 = 10;
$bimestre4 = 10;
$faltas = 40;
$aulasDadas = 50;

$media = ($bimestre1 + $bimestre2 + $bimestre3 + $bimestre4) / 4;
$porcentagemFaltas = ($faltas / $aulasDadas) * 100;

if($porcentagemFaltas > 25){
   $status = 'Reprovado'; 
}elseif ($media < 7){
    $status = 'Recuperação';
}else{
    $status = 'Aprovado';
}

echo 'Nome: ' . ($aluno  ? "$aluno<br>" : 'Aluno sem Nome<br>');
echo "Notas: $bimestre1 | $bimestre2 | $bimestre3 | $bimestre4 <br>";
echo "Média: $media<br>";
echo "Porcentagem de Faltas: $porcentagemFaltas %<br>";
echo "Status: $status";

/**
 * 1 - calcular a media aritimética da aluna
 * 2 - calcular a porcentagem de faltas
 * 3 - Exibir o seguinte relatório:[
 *   - Nome da Aluna
 *   - Notas
 *   - Média
 *   - Status 
 * * O status é definido pelas seguintes condições
 *   media >= 7 e porcentagem de faltas <= 25% : Aprovado
 *   media < 7 e porcentagem de faltas > 25% : Reprovado
 *   media < 7 e porcentagem de faltas <= 25% : Recuperação
 *   media >= 7 e porcentagem de faltas > 25% : Reprovado
*/