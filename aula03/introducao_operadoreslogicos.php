<?php

$a = 10;
$b = '10';

var_dump($a == $b);
echo '<br>';
var_dump($a === $b);
echo '<br>';
var_dump($a > $b);
echo '<br>';
var_dump($a <= $b);
echo '<br>';
var_dump($a < $b);

echo '<hr>';
var_dump($a <=> $b);
echo '<br>';
var_dump(20 <=> 10);
echo '<br>';
var_dump(10 <=> 20);