<?php

$dados = [
    'um',
    'dois',
    'tres',
    'para',
    'quatro',
    'cinco'
];

for($x = 0; $x <= 5; $x++){
    
    if($dados[$x] == 'para'){
        continue;
    }
    
    echo $dados[$x] . '<br>';
}