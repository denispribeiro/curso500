<?php
$array = [
    'Ana Paula',
    'Patricia Lima',
    'Isabela Oliveira',
    'Carolina Dias'
];

// funções para ordenação;

// sort ordena pelo valor

sort($array);

echo '<pre>';
print_r($array);

echo '<hr>';

$array2 = [
    10 => 'Ana Paula',
    7 => 'Patricia Lima',
    3 => 'Isabela Oliveira',
    1 => 'Carolina Dias'
];

// ksort ordena pela chave
ksort($array2);
echo '<pre>';
print_r($array2);

echo '<hr>';
// array_key_exists: checar se a chave do array existe
var_dump(array_key_exists('nome', [
    'nome' => 'Maria'
]));

echo '<br>';
var_dump(in_array('Maria', [
    'nome' => 'Maria'
]));

echo '<br>';
var_dump(array_search('Maria', [
    'nome' => 'Maria'
]));

echo '<hr>';
// retorna somente os valores
echo '<pre>';
print_r(array_values($array));

//retorna somente as chaves
echo '<br>';
print_r(array_keys(['nome' => 'Maria', 'email' => 'm@ig.com.br']));

//remove itens duplicados do array
$array3 = [
    'a@gmail.com',
    'b@gmail.com',
    'a@gmail.com',
    'b@gmail.com'
];

echo '<br>';
print_r(array_unique($array3));

echo '<hr>';
print_r(array_merge($array, $array2));

echo '<br>';

echo implode(', ', $array2);

//filtrando itens do array com array_filter
$array4 = [
    'a@gmail.com',
    'sdfsdf.com',
    'b@gmail.com',
    'c@gmail.com',
    'sdfsd.ig.com',
    'd@gmail.com',
    'sdfsdfuol.com'
];


echo '<hr>';
$arrayFiltrado = array_filter($array4, function($email){
    return strstr($email, '@');
});

print_r($arrayFiltrado);

echo '<hr>';
//Retornar o tamanho de um array
echo count($arrayFiltrado);



