<?php

$id = $_GET['id'] ?? null;

switch ($id) {
    case 1:
        echo "Mouse Microsoft - R$ 65,00";
        break;
    case 2:
        echo "Teclado mecânico HDTel - R$ 120,00";
        break;
    case 3:
        echo "PenDrive 120G Spell - R$ 85,00";
        break;
    case 4:
        echo "NoteBook Acer Core i7 8G - R$ 3.800,00";
        break;
    default:
        echo "Produto não cadastrado";
}