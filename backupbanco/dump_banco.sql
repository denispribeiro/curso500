--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Ubuntu 10.6-1.pgdg18.04+1)
-- Dumped by pg_dump version 11.1 (Ubuntu 11.1-3.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: alunos; Type: TABLE; Schema: public; Owner: aplicacao
--

CREATE TABLE public.alunos (
    id integer NOT NULL,
    nome character varying NOT NULL,
    serie character varying NOT NULL,
    turma character varying NOT NULL,
    curso character varying DEFAULT 'PHP'::character varying NOT NULL
);


ALTER TABLE public.alunos OWNER TO aplicacao;

--
-- Name: alunos_id_seq; Type: SEQUENCE; Schema: public; Owner: aplicacao
--

CREATE SEQUENCE public.alunos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alunos_id_seq OWNER TO aplicacao;

--
-- Name: alunos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aplicacao
--

ALTER SEQUENCE public.alunos_id_seq OWNED BY public.alunos.id;


--
-- Name: cursos; Type: TABLE; Schema: public; Owner: aplicacao
--

CREATE TABLE public.cursos (
    id integer NOT NULL,
    nome character varying NOT NULL
);


ALTER TABLE public.cursos OWNER TO aplicacao;

--
-- Name: cursos_id_seq; Type: SEQUENCE; Schema: public; Owner: aplicacao
--

CREATE SEQUENCE public.cursos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cursos_id_seq OWNER TO aplicacao;

--
-- Name: cursos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aplicacao
--

ALTER SEQUENCE public.cursos_id_seq OWNED BY public.cursos.id;


--
-- Name: usuarios; Type: TABLE; Schema: public; Owner: aplicacao
--

CREATE TABLE public.usuarios (
    id integer NOT NULL,
    email character varying NOT NULL,
    senha character varying NOT NULL,
    nome character varying NOT NULL,
    perfil integer DEFAULT 2
);


ALTER TABLE public.usuarios OWNER TO aplicacao;

--
-- Name: usuarios_id_seq; Type: SEQUENCE; Schema: public; Owner: aplicacao
--

CREATE SEQUENCE public.usuarios_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuarios_id_seq OWNER TO aplicacao;

--
-- Name: usuarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aplicacao
--

ALTER SEQUENCE public.usuarios_id_seq OWNED BY public.usuarios.id;


--
-- Name: alunos id; Type: DEFAULT; Schema: public; Owner: aplicacao
--

ALTER TABLE ONLY public.alunos ALTER COLUMN id SET DEFAULT nextval('public.alunos_id_seq'::regclass);


--
-- Name: cursos id; Type: DEFAULT; Schema: public; Owner: aplicacao
--

ALTER TABLE ONLY public.cursos ALTER COLUMN id SET DEFAULT nextval('public.cursos_id_seq'::regclass);


--
-- Name: usuarios id; Type: DEFAULT; Schema: public; Owner: aplicacao
--

ALTER TABLE ONLY public.usuarios ALTER COLUMN id SET DEFAULT nextval('public.usuarios_id_seq'::regclass);


--
-- Data for Name: alunos; Type: TABLE DATA; Schema: public; Owner: aplicacao
--

COPY public.alunos (id, nome, serie, turma, curso) FROM stdin;
1	Madalena	3 ano	2infoA	PHP
4	sfsd	sdfsdf	sdfsdfd	PHP
5	sdfsdf	sdfsdf	sdfsdfsdf	PHP
\.


--
-- Data for Name: cursos; Type: TABLE DATA; Schema: public; Owner: aplicacao
--

COPY public.cursos (id, nome) FROM stdin;
1	PHP
2	Python
3	Java Script
\.


--
-- Data for Name: usuarios; Type: TABLE DATA; Schema: public; Owner: aplicacao
--

COPY public.usuarios (id, email, senha, nome, perfil) FROM stdin;
2	amanda.souza@gmail.com	123456	Amanda Souza Lima	2
6	maria@gmail.com	123	Maria dos Santos	2
7	dpr001@teste.com	123	Denis P Ribeiro	2
9	ivan.ferro@4linux.com.br	234234	Madalena	2
1	admin@app.com	345	Administrador do Sistema1	2
12	admin@system.com	123456	Administrador Principal	1
14	mick.leal@gmail.com	123456	Michelle Santos Leal	2
\.


--
-- Name: alunos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: aplicacao
--

SELECT pg_catalog.setval('public.alunos_id_seq', 37, true);


--
-- Name: cursos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: aplicacao
--

SELECT pg_catalog.setval('public.cursos_id_seq', 3, true);


--
-- Name: usuarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: aplicacao
--

SELECT pg_catalog.setval('public.usuarios_id_seq', 14, true);


--
-- Name: alunos alunos_pkey; Type: CONSTRAINT; Schema: public; Owner: aplicacao
--

ALTER TABLE ONLY public.alunos
    ADD CONSTRAINT alunos_pkey PRIMARY KEY (id);


--
-- Name: cursos cursos_pkey; Type: CONSTRAINT; Schema: public; Owner: aplicacao
--

ALTER TABLE ONLY public.cursos
    ADD CONSTRAINT cursos_pkey PRIMARY KEY (id);


--
-- Name: usuarios usuarios_email_key; Type: CONSTRAINT; Schema: public; Owner: aplicacao
--

ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_email_key UNIQUE (email);


--
-- Name: usuarios usuarios_pkey; Type: CONSTRAINT; Schema: public; Owner: aplicacao
--

ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

