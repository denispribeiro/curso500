<?php
$aluno1 = [
    'nome' => 'Patricia Costa',
    'curso' => [
        'nome' => 'Analise de Sistemas',
        'disciplinas' => [
            'Logica de Programacao',
            'Introdução a computação',
            'Orientação a Objetos'
        ]
    ],
    'periodo' => 'Noturno'
];

$aluno2 = [
    'nome' => 'Leticia Silva',
    'curso' => [
        'nome' => 'Sistemas da Informação',
        'disciplinas' => [
            'Logica de Programacao',
            'Introdução a computação',
            'Orientação a Objetos'
        ]
    ],
    'periodo' => 'Noturno'
];

$aluno3 = [
    'nome' => 'Larissa Silveira Santos',
    'curso' => [
        'nome' => 'Tenologia em Banco de Dados',
        'disciplinas' => [
            'Logica de Programacao',
            'Introdução a computação',
            'Orientação a Objetos'
        ]
    ],
    'periodo' => 'Diurno'
];

echo '<pre>';

/*
 * A função print_r é usada
 * para imprimir a estrutura de um array
 * de uma forma mais limpa
 */
print_r($aluno1);

echo '<hr>';

echo 'Nome: ' . $aluno1['nome'];
echo '<br>Curso: ' . $aluno1['curso']['nome'];
echo '<br>Período: ' . $aluno1['periodo'];

echo '<hr>';

$alunos = [
    $aluno1,
    $aluno2,
    $aluno3
];

echo '<pre>';
print_r($alunos);

echo '<br>';

echo 'Nome: ' . $alunos[1]['nome'];

echo '<br>Curso: ' . $alunos[1]['curso']['nome'];
echo '<br>Disciplina1: ' . $alunos[1]['curso']['disciplinas'][0];
echo '<br>Disciplina1: ' . $alunos[1]['curso']['disciplinas'][1];
echo '<br>Disciplina3: ' . $alunos[1]['curso']['disciplinas'][2];

echo '<br>Período: ' . $alunos[1]['periodo'];











