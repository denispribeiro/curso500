<?php

//Toda variável no PHP inicia com o dólar

$nomePessoa   = 'Aline dos Santos';
$email  = 'aline.santos@gmail.com';
$salario = 5000.500;

echo $nomePessoa;
echo '<br>';
echo $email;
echo '<br>';
echo $salario;

//a funcão var_dump é utilizada para imprimir o valor e tipo da variável

echo '<hr>';

var_dump($nomePessoa);
echo '<br>';
var_dump($email);
echo '<br>';
var_dump($salario);

echo '<hr>';

//Convertendo tipos

$salario = (int) $salario;
var_dump($salario);

echo '<br>';
$email = (bool) $email;
var_dump($email);

echo '<br>';
$nomePessoa = (int)$nomePessoa;
var_dump($nomePessoa);













