<?php

function negrito($string)
{
    echo "<strong>$string</strong>";
}

function quebrarLinha()
{
    echo '<br>';
}

negrito("Trabalhando com Funções");
quebrarLinha();
negrito("Outra string qualquer");

echo '<hr>';

function titulo($titulo, $tamanho = 1)
{
    echo "<h$tamanho>$titulo</h$tamanho>";
}

titulo('Novo Titulo do Artigo', 6);

echo '<hr>';

function calcularMedia($n1, $n2, $n3, $n4)
{
    return ($n1 + $n2 + $n3 + $n4) / 4;
}

echo 'Media: ' . calcularMedia(10, 8, 9, 8);

echo '<hr>';

echo calcularMedia(10, 8, 9, 7) >= 7 ? 'Status: Aprovado' : 
                                       'Status: Reprovado';

