<?php
declare(strict_types = 1);

function calcularMedia(float $n1, float $n2, float $n3, float $n4):float
{
    $media = ($n1 + $n2 + $n3 + $n4) / 4;
    $media = (string) $media;
    return $media;
}

$media = calcularMedia(5, 10, 10, 10);

echo $media;

echo '<br>';

var_dump($media);