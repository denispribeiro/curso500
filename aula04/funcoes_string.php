<?php

//Funções para manipulacão de string

//Procurar valor na string

$email = 'teste@gmail.com';

$retorno = strstr($email, '@');

echo $retorno;
echo '<br>';
echo strstr($email, '@', true);

echo '<hr>';
//Retorna a posição da primeira ocorrência do valor procurado

echo strpos($email, '@');

echo '<hr>';

//Retorna parte de uma string
echo substr($email, 5);

//Retorna parte de uma string inicio e quantidade de caracteres
echo '<br>';
echo substr($email, 0, 5);
echo '<hr>';
//Retorna o tamanho da string(Quantidade de caracteres)
echo strlen($email);
echo '<br>';
echo '<hr>';
//Coverte para maiusculo e converte para minusculo
echo strtoupper($email);//Maiusculo
echo '<br>';
echo strtolower('ESSA STRING ESTÁ EM CAIXA ALTA');
echo '<hr>';
//Converte a primeira letra para maiuscula
echo ucfirst('maria');
echo '<hr>';
//Conta a quantidade de palavras
echo str_word_count('Conta quantas palavras tem na string');
echo '<hr>';
//Remove espaços em branco da string
echo trim('   sdfsdf   ');

$string = 'PHP,Python,NodeJs,Ruby';

$array  = explode(',', $string); //converte uma string para array

echo '<hr><pre>';
print_r($array);

echo '<hr>';

$string = 'Substituir qualquer coisa dentro da string';
echo "$string<br>"; 
$novaString = str_replace('qualquer coisa', 'uma string qualquer', $string);

echo $novaString;




















