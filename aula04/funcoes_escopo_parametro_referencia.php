<?php

//Escopo Glogal x Local

$nomeCompleto = 'Patricia Costa'; //Escopo Global

function nome($nome, $sobrenome)
{
    $nomeCompleto = "$nome $sobrenome"; //Escopo Local
    echo $nomeCompleto;
}

echo $nomeCompleto; //Variavel escopo Global
echo '<br>';
nome('Aline', 'Santos Silva');//A variável $nomeCompleto tem escopo local na função

$email = 'patricia.costa@gmail.com';

function relatorio()
{
    //Pega uma variável que foi setada(definida) no escopo Global
    global $nomeCompleto;
    global $email;
    
    echo "Nome: $nomeCompleto<br>";
    echo "Email: $email";
}

echo '<hr>';
relatorio();

echo '<hr>';


function verNome(&$nome)
{
    echo "Antes de Alterar: $nome<br>";
    $nome = 'Outro Nome';
    echo "Depois de Alterar: $nome<br>";
}

$nomePessoa = 'Lidia Gonzaga';

verNome($nomePessoa);

echo '<br>';

echo "Original: $nomePessoa";











