<?php

function calcularMedia(...$notas)
{
    $total = 0;
    $numeroNotas = 0;
    
    foreach ($notas as $nota){
       $total += $nota;
       $numeroNotas++;
    }
    
    $media = $total / $numeroNotas;
    return $media;
}

echo calcularMedia(10,10,10,10,10);