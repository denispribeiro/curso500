<?php require 'verifica_login.php' ;?>
<html>
	<head>
		<title>Cadastro de Alunos</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
	<body>
		<!-- http://dontpad.com/4linux/php/500/menu -->
		<?php include 'menu.php'; ?>
        <div id="main" class="container" style="margin-top:30px">
        	<div class="row">
        		<h2>+ Inserir Novo Aluno</h2>
        	</div>
        	<div class="row" style="margin-top:30px">
        	<?php 
        	   require 'conexao.php';
        	   if($_POST){
        	       if(empty($_POST['nome'])){
        	           $errorNome ='
                            <div class="alert alert-danger" role="alert">
                                O nome é obrigatório
                             </div>';
        	       }
        	       
        	       if(empty($_POST['serie'])){
        	           $errorSerie ='
                            <div class="alert alert-danger" role="alert">
                                A série do aluno é obrigatória
                             </div>';
        	       }
        	       
        	       if(empty($_POST['turma'])){
        	           $errorTurma ='
                            <div class="alert alert-danger" role="alert">
                                A turma do aluno é obrigatória
                             </div>';
        	       }
        	       
        	       $nome   = $_POST['nome'];
        	       $serie  = $_POST['serie'];
        	       $turma  = $_POST['turma'];
        	       $query = "insert into alunos
                                (nome,serie,turma)
                             values('$nome','$serie','$turma')
                            ";
        	       
        	       $result = false;
        	       
        	       if(! isset($errorNome) && 
        	          ! isset($errorSerie) &&
        	          ! isset($errorTurma)){
        	               $result = pg_exec($query);
        	               
        	               if($result){
        	                   header('location:listar_alunos.php');
        	               }else{
        	                   echo '<div class="row col-sm-10 alert alert-danger">
                                        <h5>Erro ao salvar os dados!</h5>
                                     </div>';
        	               }
        	       }
        	       
      
        	   }
        	?>
        	<form action="" method="post">
              <div class="form-group row">
                <label for="inputNome" class="col-sm-2 col-form-label">Nome</label>
                <div class="col-sm-10">
                  <input type="text" name="nome" value="<?= isset($_POST['nome']) ? $_POST['nome'] : '' ?>" class="form-control" id="inputNome3" placeholder="Nome">
                  <?= isset($errorNome) ? $errorNome : '' ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputSerie" class="col-sm-2 col-form-label">Serie</label>
                <div class="col-sm-10">
                  <input type="text" name ="serie" value="<?= isset($_POST['serie']) ? $_POST['serie'] : '' ?>" class="form-control" id="inputSerie3" placeholder="Série">
                  <?= isset($errorSerie) ? $errorSerie : '' ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputTurma" class="col-sm-2 col-form-label">Turma</label>
                <div class="col-sm-10">
                  <input type="text" name ="turma" value="<?= isset($_POST['turma']) ? $_POST['turma'] : '' ?>" class="form-control" id="inputTurma3" placeholder="Turma">
                  <?= isset($errorTurma) ? $errorTurma : '' ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPerfil" class="col-sm-2 col-form-label">Perfil</label>
                <div class="col-sm-10">
                  <select name="cursos" class="custom-select">
                      <option value="" selected>Curso</option>
                      <?php 
                        $query  = 'select * from cursos';
                        $result = pg_query($query);
                        $cursos = pg_fetch_all($result);
                        foreach ($cursos as $curso):
                      ?>
                      	<option value="<?= $curso['nome'] ?>"><?= $curso['nome'] ?></option>
                      <?php endforeach; ?>
                  </select>
                  <?= isset($errorCurso) ? $errorCurso : '' ?>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-10">
                  <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
              </div>
            </form>
        </div>
        </div>
     </body>
</html>