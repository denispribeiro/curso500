<?php

require 'verifica_login.php';

require 'conexao.php';

$id = $_GET['id'] ?? null;

if(! $id){
    echo '<h1>Id do aluno é obrigatório</h1>';
    exit();
}

$query = "delete from alunos where id=$id";

$result = pg_exec($query);

if($result){
    header('location:listar_alunos.php');
}else{
    echo '<h1>Erro ao excluir aluno</h1>';
}