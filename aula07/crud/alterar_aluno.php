<?php require 'verifica_login.php' ;?>
<html>
	<head>
		<title>Alteração de Aluno</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
	<body>
		<?php include 'menu.php'; ?>
        <div id="main" class="container" style="margin-top:30px">
        	<div class="row">
        		<h2>Alterar Aluno</h2>
        	</div>
        	<div class="row" style="margin-top:30px">
        	<?php 
        	   require 'conexao.php';
        	   
        	   $id = $_GET['id'] ?? null;
        	   
        	   $query      = "select * from alunos where id=$id";
        	   $result     = pg_query($query);
        	   $aluno    = pg_fetch_assoc($result);
        	   
        	   if($_POST){
        	       if(empty($_POST['nome'])){
        	           $errorNome ='
                            <div class="alert alert-danger" role="alert">
                                O nome é obrigatório
                             </div>';
        	       }
        	       
        	       if(empty($_POST['serie'])){
        	           $errorSerie ='
                            <div class="alert alert-danger" role="alert">
                                A série do aluno é obrigatória
                             </div>';
        	       }
        	       
        	       if(empty($_POST['turma'])){
        	           $errorTurma ='
                            <div class="alert alert-danger" role="alert">
                                A turma do aluno é obrigatória
                             </div>';
        	       }
        	       
        	       $nome   = $_POST['nome'];
        	       $serie  = $_POST['serie'];
        	       $turma  = $_POST['turma'];
        	       
        	       $query = "update alunos set nome ='$nome',
                             serie='$serie',turma='$turma'
                             where id = $id";
        	       
        	       $result = false;
        	       
        	       if(! isset($errorNome) &&
        	           ! isset($errorSerie) &&
        	           ! isset($errorTurma)){
        	               $result = pg_exec($query);
        	               
        	               if($result){
        	                   header('location:listar_alunos.php');
        	               }else{
        	                   echo '<div class="row col-sm-10 alert alert-danger">
                                        <h5>Erro ao salvar os dados!</h5>
                                     </div>';
        	               }
        	       }
        	       
        	       
        	   }
        	?>
        	<form action="" method="post">
        	  <div class="form-group row">
                <label for="inputNome" class="col-sm-2 col-form-label">Nome</label>
                <div class="col-sm-10">
                  <input type="text" name="nome" value="<?= isset($aluno['nome']) ? $aluno['nome'] : '' ?>" class="form-control" id="inputNome3" placeholder="Nome">
                  <?= isset($errorNome) ? $errorNome : '' ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputSerie" class="col-sm-2 col-form-label">Serie</label>
                <div class="col-sm-10">
                  <input type="text" name ="serie" value="<?= isset($aluno['serie']) ? $aluno['serie'] : '' ?>" class="form-control" id="inputSerie3" placeholder="Série">
                  <?= isset($errorSerie) ? $errorSerie : '' ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputTurma" class="col-sm-2 col-form-label">Turma</label>
                <div class="col-sm-10">
                  <input type="text" name ="turma" value="<?= isset($aluno['turma']) ? $aluno['turma'] : '' ?>" class="form-control" id="inputTurma3" placeholder="Turma">
                  <?= isset($errorTurma) ? $errorTurma : '' ?>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-10">
                  <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
              </div>
            </form>
        </div>
        </div>
     </body>
</html>