<?php

if($_SESSION['perfil'] != 1){
   echo '<div class="alert alert-danger" role="alert">
             Acesso negado, você não tem permissão para essa ação!
             <a href="javascript: history.go(-1)">VOLTAR</a>
         </div>';
   exit();
}