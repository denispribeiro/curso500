<?php require 'verifica_login.php' ;?>
<html>
	<head>
		<title>Listagem de Usuários</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
	<body>
		<!-- http://dontpad.com/4linux/php/500/menu -->
		<?php include 'menu.php'; ?>
		<?php require 'verifica_perfil.php'; ?>
        <div id="main" class="container" style="margin-top:30px">
        	<div class="row">
        		<h2>+ Inserir Novo Usuário</h2>
        	</div>
        	<div class="row" style="margin-top:30px">
        	<?php 
        	   require 'conexao.php';
        	   require 'utils.php';
        	   
        	   if($_POST){
        	       if(empty($_POST['nome'])){
        	           $errorNome ='
                            <div class="alert alert-danger" role="alert">
                                O nome é obrigatório
                             </div>';
        	       }
        	       
        	       if(empty($_POST['email'])){
        	           $errorEmail ='
                            <div class="alert alert-danger" role="alert">
                                O e-mail é obrigatório
                             </div>';
        	       }
        	       
        	       if(empty($_POST['senha'])){
        	           $errorSenha ='
                            <div class="alert alert-danger" role="alert">
                                A senha é obrigatório
                             </div>';
        	       }
        	       
        	       if(empty($_POST['perfil'])){
        	           $errorPerfil ='
                            <div class="alert alert-danger" role="alert">
                                O perfil é obrigatório
                             </div>';
        	       }
        	       
        	       $nome   = $_POST['nome'];
        	       $email  = $_POST['email'];
        	       $senha  = $_POST['senha'];
        	       $perfil = $_POST['perfil'];
        	       
        	       $query = "select email from 
                             usuarios where email = '$email'";
        	       
        	       $result = pg_query($query);
        	       $retorno = pg_fetch_assoc($result);
        	       
        	       if($retorno){
        	           $errorEmailDuplicado = alerta('Email já exite');
        	       }
        	       
        	       $query = "insert into usuarios
                                (nome,email,senha,perfil)
                             values('$nome','$email','$senha','$perfil')
                            ";
        	       
        	       $result = false;
        	       
        	       if(! isset($errorNome) && 
        	          ! isset($errorEmail) &&
        	          ! isset($errorSenha) &&
        	          ! isset($errorPerfil) &&
        	          ! isset($errorEmailDuplicado)){
        	               $result = pg_exec($query);
        	               
        	               if($result){
        	                   header('location:listar_usuarios.php');
        	               }else{
        	                   echo '<div class="row col-sm-10 alert alert-danger">
                                        <h5>Erro ao salvar os dados!</h5>
                                     </div>';
        	               }
        	       }
        	       
      
        	   }
        	?>
        	<?= isset($errorEmailDuplicado) ? $errorEmailDuplicado : '' ?>
        	<form action="" method="post">
              <div class="form-group row">
                <label for="inputNome" class="col-sm-2 col-form-label">Nome</label>
                <div class="col-sm-10">
                  <input type="text" name="nome" value="<?= isset($_POST['nome']) ? $_POST['nome'] : '' ?>" class="form-control" id="inputNome3" placeholder="Nome">
                  <?= isset($errorNome) ? $errorNome : '' ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                  <input type="email" name ="email" value="<?= isset($_POST['email']) ? $_POST['email'] : '' ?>" class="form-control" id="inputEmail3" placeholder="Email">
                  <?= isset($errorEmail) ? $errorEmail : '' ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">Senha</label>
                <div class="col-sm-10">
                  <input type="password" name="senha" value="<?= isset($_POST['senha']) ? $_POST['senha'] : '' ?>" class="form-control" id="inputPassword3" placeholder="Senha">
                  <?= isset($errorSenha) ? $errorSenha : '' ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPerfil" class="col-sm-2 col-form-label">Perfil</label>
                <div class="col-sm-10">
                  <select name="perfil" class="custom-select">
                      <option value="" selected>Perfil</option>
                      <option value="1">Administrador</option>
                      <option value="2">Secretaria</option>
                  </select>
                  <?= isset($errorPerfil) ? $errorPerfil : '' ?>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-10">
                  <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
              </div>
            </form>
        </div>
        </div>
     </body>
</html>