<?php require 'verifica_login.php' ;?>
<html>
	<head>
		<title>Listagem de Alunos</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
	<body>
		<?php include 'menu.php'; ?>
        <div id="main" class="container" style="margin-top:30px">
        	<p><h2>Lista de Alunos</h2></p>
        	<div class="row">
            	<table class="table">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Nome</th>
                      <th scope="col">Série</th>
                      <th scope="col">Turma</th>
                      <th scope="col">Ações</th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php 
                        //Inicio do Fluxo
                  	    //http://dontpad.com/4linux/php/500/listar_alunos
                        require 'conexao.php';
                        $query  = 'select * from alunos order by id';
                        $result = pg_query($query);
                        $alunos = pg_fetch_all($result);
                        foreach ($alunos as $aluno):
                    ?>
                    <tr>
                      <th scope="row"><?= $aluno['id'] ?></th>
                      <td><?= $aluno['nome'] ?></td>
                      <td><?= $aluno['serie'] ?></td>
                      <td><?= $aluno['turma'] ?></td>
                      <td>
                      	<a href="alterar_aluno.php?id=<?= $aluno['id'] ?>">Alterar</a> | 
                      	<a href="excluir_aluno.php?id=<?= $aluno['id'] ?>">Excluir</a>
                      </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
             </div>
        </div>
	</body>
</html>