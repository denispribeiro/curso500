<?php 
    $acao = '';
    if($_SESSION['perfil'] != 1){
       $acao = 'disabled'; 
    }

?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<div class="collapse navbar-collapse" id="navbarNav">
		<ul class="navbar-nav">
			<li class="nav-item"><a class="nav-link  <?= $acao?>"
				href="listar_usuarios.php">USUÁRIOS<span class="sr-only">(current)</span></a>
			</li>
			<li class="nav-item"><a class="nav-link  <?= $acao?>" href="cadastrar_usuario.php">CADASTRAR
					USUÁRIO</a></li>
			<li class="nav-item"><a class="nav-link" href="listar_alunos.php">ALUNOS</a></li>
			<li class="nav-item"><a class="nav-link" href="cadastrar_aluno.php">CADASTRAR ALUNO</a>
			</li>
			<li class="nav-item">
				<p class="nav-link" style="margin-left: 60px">
                	<?php
                if (isset($_SESSION['nomeUsuario'])) {
                    echo ' | ' . $_SESSION['nomeUsuario'];
                    echo ' | <a href="alterar_senha.php">Alterar Senha</a>';
                    echo ' | <a href="sair.php">Sair</a>';
                }
                ?>
                </p>
			</li>
		</ul>
	</div>
</nav>