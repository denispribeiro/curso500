<html>
	<head>
		<title>Autenticação</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link disabled" href="listar_usuarios.php">USUÁRIOS<span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link disabled" href="#">CADASTRAR USUÁRIO</a>
              </li>
              <li class="nav-item">
                <a class="nav-link disabled" href="#">ALUNOS</a>
              </li>
              <li class="nav-item">
                <a class="nav-link disabled" href="#">CADASTRAR ALUNOS</a>
              </li>
              <li class="nav-iteam">
                <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
              </li>
            </ul>
          </div>
        </nav>
        <div id="main" class="container col-sm-3 bg-light" style="margin-top:80px">
        	<div class="row">
        		<h2>Autenticação</h2>
        	</div>
        	<div class="row" style="margin-top:30px; padding:5px">
        	<?php 
        	   require 'conexao.php';
        	   
        	   if($_POST){
        	       $email = $_POST['email'];
        	       $senha = $_POST['senha'];
        	       $query = "select * from usuarios
                              where email='$email' and senha = '$senha'
                            ";
        	       
        	       $result  = pg_query($query);
        	       $usuario = pg_fetch_assoc($result);
        	       
        	       if(! $usuario){
        	           echo '<div class="row col-sm-10 alert alert-danger">
                               <h5>Usuário ou senha incorretos</h5>
                              </div>';
        	       }else{
        	           session_start();
        	           $_SESSION['logado'] = true;
        	           $_SESSION['nomeUsuario'] = $usuario['nome'];
        	           $_SESSION['perfil'] = $usuario['perfil'];
        	           $_SESSION['id'] = $usuario['id'];
        	           if($usuario['perfil'] == 1){
        	               header('location:listar_usuarios.php');
        	           }else{
        	               header('location:listar_alunos.php');
        	           }
        	       }
        	   }
        	?>
        	<form action="" method="post">
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                  <input type="email" name ="email" value="<?= isset($_POST['email']) ? $_POST['email'] : '' ?>" class="form-control" id="inputEmail3" placeholder="Email">
                  <?= isset($errorEmail) ? $errorEmail : '' ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">Senha</label>
                <div class="col-sm-10">
                  <input type="password" name="senha" value="<?= isset($_POST['senha']) ? $_POST['senha'] : '' ?>" class="form-control" id="inputPassword3" placeholder="Senha">
                  <?= isset($errorSenha) ? $errorSenha : '' ?>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-10">
                  <button type="submit" class="btn btn-primary">Logar</button>
                </div>
              </div>
            </form>
        </div>
        </div>
     </body>
</html>