<?php

require 'verifica_login.php';
require 'verifica_perfil.php';

require 'conexao.php';

$id = $_GET['id'] ?? null;

if(! $id){
    echo '<h1>Id do usuario é obrigatório</h1>';
    exit();
}

$query = "delete from usuarios where id=$id";

$result = pg_exec($query);

if($result){
    header('location:listar_usuarios.php');
}else{
    echo '<h1>Erro ao excluir usuario</h1>';
}