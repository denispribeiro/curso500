<?php

function alerta($texto, $tipo = 'danger'){
    
    $alerta = "<div class='row col-sm-10 alert alert-$tipo'>
                  <h5>$texto</h5>
               </div>";
    
    return $alerta;
}